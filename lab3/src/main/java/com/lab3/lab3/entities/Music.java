package com.lab3.lab3.entities;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table (name="MUSICS")
public class Music {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;
    @Column (name="TITLE")
    private String title;
    @Column (name="ARTIST")
    private String artist;
    @Column (name="GENDER")
    private String gender;
    @Column (name="DURATION")
    private String duration;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
