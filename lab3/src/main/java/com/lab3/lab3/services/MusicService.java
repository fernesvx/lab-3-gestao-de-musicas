package com.lab3.lab3.services;

import com.lab3.lab3.entities.Music;
import com.lab3.lab3.repositories.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MusicService {
    @Autowired
    MusicRepository musicRepository;

    public Music create(Music music){
        return musicRepository.save(music);
    }

    public void remove(UUID id){
        musicRepository.deleteById(id);
    }

    public Music update(Music music, UUID id){
        if(musicRepository.existsById(id)){
            music.setUuid(id);
            return musicRepository.save(music);
        }
        return null;
    }

    public List<Music> getAllMusics(){
        return musicRepository.findAll();
    }

    public List<Music> getByTitle(String title){
        return musicRepository.findByTitle(title);
    }

    public List<Music> getByArtist(String artist){
        return musicRepository.findByArtist(artist);
    }

    public List<Music> getByGender(String gender){
        return musicRepository.findByGender(gender);
    }

}