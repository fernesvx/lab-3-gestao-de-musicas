package com.lab3.lab3.repositories;

import com.lab3.lab3.entities.Music;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MusicRepository extends JpaRepository<Music, UUID> {
    List<Music> findByTitle(String title);
    List<Music> findByArtist(String artist);
    List<Music> findByGender(String gender);

}