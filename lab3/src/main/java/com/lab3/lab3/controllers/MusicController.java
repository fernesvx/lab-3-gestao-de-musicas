package com.lab3.lab3.controllers;

import com.lab3.lab3.entities.Music;
import com.lab3.lab3.services.MusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/musics")
public class MusicController {
    @Autowired
    MusicService musicService;

    @PostMapping
    public ResponseEntity<Music> create(@RequestBody Music music){
        return ResponseEntity.status(HttpStatus.CREATED).body(musicService.create(music));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Music> update(@PathVariable UUID id, @RequestBody Music music){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.update(music, id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Music> remove(@PathVariable UUID id){
        musicService.remove(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/all")
    public ResponseEntity<List<Music>> getAllMusics(){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.getAllMusics());
    }

    @GetMapping("/title/{title}")
    public ResponseEntity<List<Music>> getByTitle(@PathVariable String title){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.getByTitle(title));
    }

    @GetMapping("/artist/{artist}")
    public ResponseEntity<List<Music>> getByArtist(@PathVariable String artist){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.getByArtist(artist));
    }

    @GetMapping("/gender/{gender}")
    public ResponseEntity<List<Music>> getByGender(@PathVariable String gender){
        return ResponseEntity.status(HttpStatus.OK).body(musicService.getByGender(gender));
    }

}
